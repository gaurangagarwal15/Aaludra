import './App.css';
import Navbar from './components/Navbar';
import Accordian from './components/Accordian';
import ConsultingForm from './components/Consultingform';
import OfferforemploymentForm from './components/OfferForEmployment';
import FreshOfferForm from './components/FreshOffer';
import AppraisalForm from './components/Appraisal';
import ReleavingForm from './components/Releaving';
import ExperienceForm from './components/ExperinceLetter';
function App() {
  return (
    <>
      <Navbar />
      <div className="container">
        <br />
        <h2>HR Letters</h2>        
        <br />
          <Accordian
            id="1"
            uid="a"
            heading="CONSULTING OFFER"
            expandComponent={<ConsultingForm />}
          />
          <br />
          <Accordian
            id="2" 
            uid="b"
            heading="OFFER FOR EMPLOYMENT: - LATERALS"
            expandComponent={<OfferforemploymentForm />}
          />
          <br />
          <Accordian 
            id="3"
            uid="c"
            heading="FRESHERS OFFER LETTER"
            expandComponent={<FreshOfferForm />}
          />
          <br />
          <Accordian 
            id="4"
            uid="d"
            heading="APPRAISAL LETTER"
            expandComponent={<AppraisalForm />}
          />
          <br />
          <Accordian 
            id="5"
            uid="e"
            heading="RELEAVING LETTER"
            expandComponent={<ReleavingForm />}
          />
          <br />
          <Accordian 
            id="6"
            uid="f"
            heading="EXPERIENCE LETTER"
            expandComponent={<ExperienceForm />}
          />
      </div>
    </>
  );
}

export default App;
