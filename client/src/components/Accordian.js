import './defaultstyles/accordian.css'
const Accordian = (props) =>{
    return(
        <div className="accordion" id="accordionExample">
            <div className="accordion-item">
                <h2 className="accordion-header" id="headingTwo">
                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target={'#'+props.uid} aria-expanded="false" aria-controls="collapseTwo">
                    {props.heading}
                </button>
                </h2>
                <div id={props.uid} className="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                    <div className="accordion-body">
                        {props.expandComponent}
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Accordian;