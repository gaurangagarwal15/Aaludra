import React,{useState} from "react";
import './defaultstyles/consultingform.css'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()


const Form = () =>{

    const [date,setDate] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [title, setTitle] = useState(-1)
    const [employeeID,setEmployeeID] = useState(0)
    const [lwdSalary, setLwdSalary] = useState(0)
    const [role, setRole] = useState('')
    const [fromDate, setFromDate] = useState('')
    const [toDate, setToDate] = useState('')

    const handleSubmit = (e) =>{
        e.preventDefault();
        // access state variables here
    };


    return(
        <>
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label for="date" class="form-label">Date</label>
                <input className="form-control" id="date" type="date" name="date" value={date} onChange={(e)=>setDate(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="fullname" class="form-label">Full name</label>
                <div id="fullname" className="input-group">
                    <select className="form-select" id="inputGroupSelect04" aria-label="Example select with button addon" name="nameTitle" value={title} onChange={(e)=>setTitle(e.target.value)}>
                        <option selected>Title</option>
                        <option value="1">Mr</option>
                        <option value="2">Ms</option>
                        <option value="3">Mrs</option>
                    </select>
                    <input type="text" aria-label="First name" placeholder="First Name" class="form-control" value={firstName} onChange={(e)=>setFirstName(e.target.value)} />
                    <input type="text" aria-label="Last name" placeholder="Last Name" class="form-control" value={lastName} onChange={(e)=>setLastName(e.target.value)} />
                </div>
            </div>
            <div className="mb-3">
                <label for="pincode" class="form-label">Employee ID</label>
                <input className="form-control" id="empID" type="number" value={employeeID} onChange={(e)=>setEmployeeID(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="sdate" class="form-label">From Date</label>
                <input className="form-control" id="fromDate" type="date"  value={fromDate} onChange={(e)=>setFromDate(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="sdate" class="form-label">To Date</label>
                <input className="form-control" id="toDate" type="date" value={toDate} onChange={(e)=>setToDate(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="role" class="form-label">Role</label>
                <input className="form-control" id="role" type="text" name="role" value={role} onChange={(e)=>setRole(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="pincode" class="form-label">Last Drawn Salary</label>
                <input className="form-control" id="lwdSalary" type="number" value={lwdSalary} onChange={(e)=>setLwdSalary(e.target.value)} />
            </div>
            <br />
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        </>
    );
}
export default Form