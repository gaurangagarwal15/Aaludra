const Toast = () =>{
    return(
        <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
            <div class="toast-header">
                <strong class="mr-auto">{props.heading}</strong>
                <small class="text-muted">{props.smallDetail}</small>
                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                {props.message}
            </div>
        </div>
    );
}

export default Toast