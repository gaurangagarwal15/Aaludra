import React,{useState} from "react";
import './defaultstyles/consultingform.css'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()


const Form = () =>{

    const [date,setDate] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [title, setTitle] = useState(0)
    const [role, setRole] = useState('')
    const [effectiveDate, setEffectiveDate] = useState('')
    const [ctc, setCtc] =  useState(0)
    const [basicSalary,setBasicSalary] = useState(0)
    const [hra, setHra] = useState(0)
    const [convA, setConvA] = useState(0)
    const [projA, setProjA] = useState(0)
    const [perfA, setPerfA] = useState(0)
    const [totalMonthly, setTotalMonthly] = useState(0)
    const [totalAnnual, setTotalAnnual] = useState(0)



    const handleSubmit = (e) =>{
        e.preventDefault();
        // access state variables here
    };


    return(
        <>
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label for="date" class="form-label">Date</label>
                <input className="form-control" id="date" type="date" name="date" value={date} onChange={(e)=>setDate(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="fullname" class="form-label">Full name</label>
                <div id="fullname" className="input-group">
                    <select className="form-select" id="inputGroupSelect04" aria-label="Example select with button addon" name="nameTitle" value={title} onChange={(e)=>setTitle(e.target.value)}>
                        <option selected>Title</option>
                        <option value="1">Mr</option>
                        <option value="2">Ms</option>
                        <option value="3">Mrs</option>
                    </select>
                    <input type="text" aria-label="First name" placeholder="First Name" class="form-control" value={firstName} onChange={(e)=>setFirstName(e.target.value)} />
                    <input type="text" aria-label="Last name" placeholder="Last Name" class="form-control" value={lastName} onChange={(e)=>setLastName(e.target.value)} />
                </div>
            </div>
            <div className="mb-3">
                <label for="role" class="form-label">Role</label>
                <input className="form-control" id="role" type="text" value={role} onChange={(e)=>setRole(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="effectiveDate" class="form-label">Effective Date</label>
                <input className="form-control" id="effectiveDate" type="date" value={effectiveDate} onChange={(e)=>setEffectiveDate(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="ctc" class="form-label">CTC</label>
                <input className="form-control" id="ctc" type="number" value={ctc} onChange={(e)=>setCtc(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="basicSalary" class="form-label">Basic Salary</label>
                <input className="form-control" id="basicSalary" type="number"  value={basicSalary} onChange={(e)=>setBasicSalary(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="hra" class="form-label">HRA</label>
                <input className="form-control" id="hra" type="number" value={hra} onChange={(e)=>setHra(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="convA" class="form-label">Conveyance Allowance</label>
                <input className="form-control" id="convA" type="number"  value={convA} onChange={(e)=>setConvA(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="projA" class="form-label">Project Allowance</label>
                <input className="form-control" id="projA" type="number"  value={projA} onChange={(e)=>setProjA(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="perfA" class="form-label">Performance Allowance</label>
                <input className="form-control" id="perfA" type="number"  value={perfA} onChange={(e)=>setPerfA(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="totalMonthly" class="form-label">Total Monthly</label>
                <input className="form-control" id="totalMonthly" type="number" value={totalMonthly} onChange={(e)=>setTotalMonthly(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="totalAnnual" class="form-label">Total Annualized</label>
                <input className="form-control" id="totalAnnual" type="number"  value={totalAnnual} onChange={(e)=>setTotalAnnual(e.target.value)} />
            </div>
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        </>
    );
}
export default Form