import React,{useState} from "react";
import './defaultstyles/consultingform.css'
import {toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure()


const Form = () =>{

    const [date,setDate] = useState('')
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [title, setTitle] = useState(-1)
    const [address,setAddress] = useState('')
    const [pinCode, setPincode] = useState()
    const [role, setRole] = useState('')
    const [gepm, setGepm] = useState('')
    const [startDate, setStartDate] = useState('')

    const handleChange = (event) => {

        const firstNameLength = firstName.length < 20;

        if(firstNameLength){setFirstName(event.target.value)}

    }


    const handleSubmit = (e) =>{
        e.preventDefault();
        // access state variables here
    };


    return(
        <>
        <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label for="date" class="form-label">Date</label>
                <input className="form-control" id="date" type="date" name="date" value={date} onChange={(e)=>setDate(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="fullname" class="form-label">Full name</label>
                <div id="fullname" className="input-group">
                    <select className="form-select" id="inputGroupSelect04" aria-label="Example select with button addon" name="nameTitle" value={title} onChange={(e)=>setTitle(e.target.value)}>
                        <option selected>Title</option>
                        <option value="1">Mr</option>
                        <option value="2">Ms</option>
                        <option value="3">Mrs</option>
                    </select>
                    <input type="text" aria-label="First name" placeholder="First Name" class="form-control" value={firstName} onChange={handleChange} />
                    <input type="text" aria-label="Last name" placeholder="Last Name" class="form-control" value={lastName} onChange={(e)=>setLastName(e.target.value)} />
                </div>
            </div>
            <div className="mb-3">
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Address</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" onChange={(e)=>setAddress(e.target.value)}>{address}</textarea>
                </div>
            </div>
            <div className="mb-3">
                <label for="pincode" class="form-label">Pincode</label>
                <input className="form-control" id="pincode" type="number"  value={pinCode} onChange={(e)=>setPincode(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="role" class="form-label">Role</label>
                <input className="form-control" id="role" type="text" value={role} onChange={(e)=>setRole(e.target.value)}/>
            </div>
            <div className="mb-3">
                <label for="grossEmol" class="form-label">Gross Emoluments</label>
                <input className="form-control" id="grossEmol" type="number" value={gepm} onChange={(e)=>setGepm(e.target.value)} />
            </div>
            <div className="mb-3">
                <label for="sdate" class="form-label">Start Date</label>
                <input className="form-control" id="sdate" type="date" value={startDate} onChange={(e)=>setStartDate(e.target.value)}/>
            </div>
            <br />
            <button type="submit" className="btn btn-primary">Submit</button>
        </form>
        </>
    );
}
export default Form